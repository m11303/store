FROM registry.gitlab.com/m11303/wrapper
USER root
WORKDIR /app
USER 1000
COPY requirements.txt /app
RUN pip install --no-cache-dir --upgrade -r requirements.txt
USER root
COPY . /app
USER 1000


ENTRYPOINT [ "python3" ]
CMD [ "wrapper.py" ]
